<?php

namespace Drupal\mixpanel\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Mixpanel  settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'mixpanel_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['mixpanel.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['project_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Project Token'),
      '#default_value' => $this->config('mixpanel.settings')->get('project_token') ?? '',
    ];
    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#default_value' => $this->config('mixpanel.settings')->get('host') ?? '',
      '#description' => $this->t('Can be used to configure EU data residency as indicated in <a href=":url" target="_blank">Mixpanel documentation</a>. Safe to leave blank.<br/>
            <em>Only</em> include the domain, not the schema, e.g. "example.com" not "https://example.org"', [':url' => 'https://developer.mixpanel.com/docs/php'])
    ];
    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => 'Debug mode',
      '#default_value' => $this->config('mixpanel.settings')->get('debug') ?? FALSE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('mixpanel.settings')
      ->set('project_token', $form_state->getValue('project_token'))
      ->set('host', $form_state->getValue('host'))
      ->set('debug', $form_state->getValue('debug'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
