<?php

namespace Drupal\mixpanel;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Service description.
 */
class MixpanelClient {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A configured Mixpanel client.
   *
   * @var \Mixpanel
   */
  protected $client;

  /**
   * Constructs a MixpanelClient object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Get a configured Mixpanel client.
   *
   * @return \Mixpanel
   */
  public function getClient(): \Mixpanel {
    if ($this->client) {
      return $this->client;
    }

    $settings = $this->configFactory->get('mixpanel.settings');
    $projectToken = $settings->get('project_token');
    $options = [];
    if ($host = $settings->get('host')) {
      $options['host'] = $host;
    }
    if ($settings->get('debug')) {
      $options['debug'] = TRUE;
    }
    $this->client = \Mixpanel::getInstance($projectToken, $options);

    return $this->client;
  }

}
